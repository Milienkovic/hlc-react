import React, {useRef} from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";
import Home from "./ui/pages/Home/Home";
import Paths from "./core/utils/Paths";
import Stats from "./ui/components/common/Stats/Stats";
import useStorePersistence from "./core/hooks/useStorePersistence";

import './App.css';

function App() {
    const homeRef = useRef(null);
    const {store} = useSelector(store => ({
        store: store,
    }));

    useStorePersistence(store);

    return (
        <div ref={homeRef}>
            <Stats/>
            <Switch>
                <Route exact path={Paths.HOME} component={Home}/>
                <Redirect to={Paths.HOME}/>
            </Switch>
        </div>
    );
}

export default App;
