/* eslint-disable react-hooks/exhaustive-deps */
import Card from "../cards/models/card";
import {useCallback} from "react";

const useCards = () => {

    const cardName = (index, type) => {
        if (index === 9) return `ace of ${type}s`;
        if (index === 10) return `jack of ${type}s`;
        if (index === 11) return `queen of ${type}s`;
        if (index === 12) return `king of ${type}s`;
        return `${index + 2} of ${type}s`;
    }

    const cardImage = (index, type) => {
        if (index === 9) return require(`../../assets/images/cards/${type}s/A ${type}s.png`);
        if (index === 10) return require(`../../assets/images/cards/${type}s/J ${type}s.png`);
        if (index === 11) return require(`../../assets/images/cards/${type}s/Q ${type}s.png`);
        if (index === 12) return require(`../../assets/images/cards/${type}s/K ${type}s.png`);
        return require(`../../assets/images/cards/${type}s/${index + 2} ${type}s.png`);
    }

    const createCard = (index, type) => {
        return new Card(cardName(index, type), index + 2, cardImage(index, type));
    }

    const initDeck = useCallback(() => {
        const cards = [];
        for (let i = 0; i < 13; i++) {
            const spade = createCard(i, 'spade');
            const diamond = createCard(i, 'diamond');
            const heart = createCard(i, 'heart');
            const club = createCard(i, 'club');
            cards.push(spade);
            cards.push(diamond);
            cards.push(heart);
            cards.push(club);
        }
        return cards;
    }, []);

    const shuffledDeck = useCallback(() => {
        const cards = initDeck();
        for (let i = 0; i < cards.length; i++) {
            const index = Math.floor(Math.random() * cards.length);
            const temp = cards[i];
            cards[i] = cards[index];
            cards[index] = temp;
        }
        return cards;
    }, []);

    return {initDeck, shuffledDeck};
}

export default useCards;