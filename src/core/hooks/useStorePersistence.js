import {useEffect} from "react";
import {persistedGame} from "../redux/actions/gameActions";
import {persistedPlayer} from "../redux/actions/playerActions";
import {persistedConfig} from "../redux/actions/generalActions";
import {useDispatch} from "react-redux";

const useStorePersistence = store => {

    const dispatch = useDispatch();

    useEffect(() => {
        if (localStorage.getItem('player') && localStorage.getItem('game') && localStorage.getItem('general')) {
            dispatch(persistedGame());
            dispatch(persistedPlayer());
            dispatch(persistedConfig());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        const storeTheStore = () => {
            for (const key in store) {
                if (store.hasOwnProperty(key)) {
                    localStorage.setItem(key, JSON.stringify(store[key]));
                }
            }
        }

        window.addEventListener('beforeunload', storeTheStore);

        return () => {
            window.removeEventListener('beforeunload', storeTheStore)
        }
    }, [store]);
}

export default useStorePersistence;