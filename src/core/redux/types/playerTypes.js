export default class PlayerTypes {
    static SET_COINS = 'SET_COINS';
    static PLACE_BET = 'PLACE_BET';
    static PERSISTED_PLAYER = 'PERSISTED_PLAYER';
    static NEW_GAME_PLAYER ='NEW_GAME_PLAYER';
    static RESET_PLAYER = 'RESET_PLAYER';
}