export default class GameTypes {
    static SET_CARDS = 'SET_CARDS';
    static UPDATE_OPEN_CARDS = 'UPDATE_OPEN_CARDS';
    static SET_PREVIOUS_CARD = 'SET_PREVIOUS_CARD';
    static SET_INDEX = 'SET_INDEX';
    static GAME_OVER = 'GAME_OVER';
    static PLAY = 'PLAY';
    static NEW_GAME = 'NEW_GAME';
    static RESTART = 'RESTART';
    static PERSISTED_GAME = 'PERSISTED_GAME';
}