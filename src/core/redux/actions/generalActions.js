import GeneralTypes from "../types/generalTypes";

export const setDimensions = dimensions => {
    return {
        type: GeneralTypes.SET_DIMENSIONS,
        dimensions,
    }
};

export const displayBetControls = display => {
    return {
        type: GeneralTypes.DISPLAY_BET_CONTROLS,
        display,
    }
};

export const pageRefreshed = isRefreshed => {
    return {
        type: GeneralTypes.PAGE_REFRESHED,
        isRefreshed,
    }
};

export const persistedConfig = () => {
    const generalString = localStorage.getItem('general');
    const general = JSON.parse(generalString);
    localStorage.removeItem('general');
    return {
        type: GeneralTypes.PERSISTED_CONFIG,
        general,
    }
}

