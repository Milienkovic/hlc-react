import PlayerTypes from "../types/playerTypes";
import playerTypes from "../types/playerTypes";

export const setCoins = coins => {
    return {
        type: PlayerTypes.SET_COINS,
        coins,
    }
};

export const placeBet = bet => {
    return {
        type: PlayerTypes.PLACE_BET,
        bet,
    }
};

export const newGamePlayer = coins => {
    return {
        type: playerTypes.NEW_GAME_PLAYER,
        coins,
    }
};

export const resetPlayer = () => {
    return {
        type: PlayerTypes.RESET_PLAYER,
    }
}

export const persistedPlayer = () => {
    const playerString = localStorage.getItem('player');
    const player = JSON.parse(playerString);
    localStorage.removeItem('player')
    return {
        type: PlayerTypes.PERSISTED_PLAYER,
        player
    }
};

