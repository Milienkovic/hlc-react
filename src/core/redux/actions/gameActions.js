import GameTypes from "../types/gameTypes";

export const setIndex = index => {
    return {
        type: GameTypes.SET_INDEX,
        index
    }
};

export const setCards = cards => {
    return {
        type: GameTypes.SET_CARDS,
        cards
    }
};

export const updateOpenCards = card => {
    return {
        type: GameTypes.UPDATE_OPEN_CARDS,
        card
    }
};

export const play = () => {
    return {
        type: GameTypes.PLAY,
    }
}

export const gameOver = () => {
    return {
        type: GameTypes.GAME_OVER,
    }
};

export const setPreviousCard = card => {
    return {
        type: GameTypes.SET_PREVIOUS_CARD,
        card
    }
};

export const restart = cards => {
    return {
        type: GameTypes.RESTART,
        cards,
    }
};

export const newGame = cards => {
    return {
        type: GameTypes.NEW_GAME,
        cards,
    }
};

export const persistedGame = () => {
    const gameString = localStorage.getItem('game');
    const persistedGame = JSON.parse(gameString);
    let game = persistedGame;
    if (persistedGame && persistedGame.cards.length === 0) {
        console.log('no cards')
    }

    localStorage.removeItem('game');
    return {
        type: GameTypes.PERSISTED_GAME,
        game
    }
}

