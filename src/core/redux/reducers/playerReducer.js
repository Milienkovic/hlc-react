import PlayerTypes from "../types/playerTypes";
import BetOptions from "../../utils/BetOptions";

const initState = {
    coins: 100,
    currentBet: 10,
    isWin: false,
}

const restart = () => {
    return {
        ...initState,
    }
}

const newGame = (state, action) => {
    return {
        ...state,
        coins: action.coins
    }
}

const manageCoins = (coins, bet, isWin) => {
    if (isWin) {
        return coins + bet * 2;
    } else {
        return coins - bet;
    }
};

const placeBet = (state, action) => {
    const {guess, amount, previousCard, nextCard} = action.bet;
    const isHigher = guess === BetOptions.HIGHER && nextCard > previousCard;
    const isLower = guess === BetOptions.LOWER && nextCard <= previousCard;
    const isWin = isHigher || isLower;
    const coins = manageCoins(state.coins, amount, isWin)

    return {
        ...state,
        coins: coins,
        currentBet: amount,
        isWin
    }
};

const setCoins = (state, action) => {
    return {
        ...state,
        coins: action.coins,
    }
};

const persistedPlayer = (state, action) => {
    return action.player;
};

const playerReducer = (state = initState, action) => {
    switch (action.type) {
        case PlayerTypes.PLACE_BET:
            return placeBet(state, action);
        case PlayerTypes.SET_COINS:
            return setCoins(state, action);
        case PlayerTypes.RESET_PLAYER:
            return restart();
        case PlayerTypes.NEW_GAME_PLAYER:
            return newGame(state, action);
        case PlayerTypes.PERSISTED_PLAYER:
            return persistedPlayer(state, action);
        default:
            return state;
    }
}

export default playerReducer;