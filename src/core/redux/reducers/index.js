import {combineReducers} from "redux";
import generalReducer from "./generalReducer";
import gameReducer from "./gameReducer";
import playerReducer from "./playerReducer";

export const rootReducer = combineReducers({
    general: generalReducer,
    game: gameReducer,
    player: playerReducer,
});