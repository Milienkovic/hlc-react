import GeneralTypes from "../types/generalTypes";

const initState = {
    dimensions: {width: 0, height: 0},
    displayBetControls: false,
    isRefreshed: false
};

const setDimensions = (state, action) => {
    return {
        ...state,
        dimensions: action.dimensions
    }
};

const displayBetControls = (state, action) => {
    return {
        ...state,
        displayBetControls: action.display,
    }
};

const pageRefreshed = (state, action) => {
    return {
        ...state,
        isRefreshed: action.isRefreshed
    }
};

const persistedConfig = (state, action) => {
    return action.general;
};

const generalReducer = (state = initState, action) => {
    switch (action.type) {
        case GeneralTypes.SET_DIMENSIONS:
            return setDimensions(state, action);
        case GeneralTypes.DISPLAY_BET_CONTROLS:
            return displayBetControls(state, action);
        case GeneralTypes.PAGE_REFRESHED:
            return pageRefreshed(state, action);
        case GeneralTypes.PERSISTED_CONFIG:
            return persistedConfig(state, action);
        default:
            return state;
    }
}

export default generalReducer;