import GameTypes from "../types/gameTypes";

const initState = {
    cards: [],
    previousCard: null,
    openCards: [],
    index: 51,
    isPlaying: false,
    isGameOver: false,
}

const play = (state, action) => {
    return {
        ...state,
        isPlaying: true,
        isGameOver: false,
    }
}

const gameOver = (state, action) => {
    return {
        ...state,
        isGameOver: true,
        isPlaying: false,
        cards: [],
        previousCard: null,
        openCards: [],
        index: 51,
    }
};

const setCards = (state, action) => {
    return {
        ...state,
        cards: action.cards
    }
};

const updateOpenCards = (state, action) => {
    return {
        ...state,
        openCards: [...state.openCards, action.card]
    }
};

const setPreviousCard = (state, action) => {
    return {
        ...state,
        previousCard: action.card,
    }
};

const setIndex = (state, action) => {
    return {
        ...state,
        index: action.index,
    }
};

const restart = (state, action) => {
    return {
        ...state,
        previousCard: null,
        openCards: [],
        index: 51,
        isPlaying: true,
        isGameOver: false,
        cards: action.cards
    }
};

const newGame = (state, action) => {
    return {
        ...state,
        previousCard: null,
        openCards: [],
        index: 51,
        isPlaying: true,
        isGameOver: false,
        cards: action.cards
    }
};

const persistedGame = (state, action) => {
    return action.game;
};

const gameReducer = (state = initState, action) => {
    switch (action.type) {
        case GameTypes.PLAY:
            return play(state, action);
        case GameTypes.SET_CARDS:
            return setCards(state, action);
        case GameTypes.UPDATE_OPEN_CARDS:
            return updateOpenCards(state, action);
        case GameTypes.SET_INDEX:
            return setIndex(state, action);
        case GameTypes.GAME_OVER:
            return gameOver(state, action);
        case GameTypes.SET_PREVIOUS_CARD:
            return setPreviousCard(state, action);
        case GameTypes.RESTART:
            return restart(state, action);
        case GameTypes.NEW_GAME:
            return newGame(state, action);
        case GameTypes.PERSISTED_GAME:
            return persistedGame(state, action);
        default:
            return state;
    }
}

export default gameReducer;