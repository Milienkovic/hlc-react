export default class Card{

    constructor(name, value, image) {
        this.name = name;
        this.value = value;
        this.image = image;
    }
}