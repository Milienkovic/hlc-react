import React, {useState} from 'react';
import {Group} from "react-konva";
import {useSelector} from "react-redux";
import {animated, Spring} from "@react-spring/konva";

const WinIndicator = () => {
    const {dimensions, isWin, currentBet, index} = useSelector(state => ({
        dimensions: state.general.dimensions,
        isWin: state.player.isWin,
        currentBet: state.player.currentBet,
        index: state.game.index
    }));

    const handleMouseEnter = event => {
        const container = event.target.getStage().container();
        container.style.cursor = 'pointer';
    }

    const handleMouseLeave = event => {
        const container = event.target.getStage().container();
        container.style.cursor = 'default';
    }

    const [flag, setFlag] = useState(false);
    const handleClick = () => setFlag(prevState => !prevState);

    const renderText = () => {
        return <Spring
            from={{
                opacity: 0,
                fontSize: 0,
                x: 0,
                fill: 'rgb(0, 0 ,0)',
                shadowBlur: 5,
                shadowColor: isWin ? 'darkgreen' : 'red'
            }}
            to={{
                opacity: flag ? 1 : 0,
                x: flag ? dimensions.width * 0.9 - 100 - dimensions.width * 0.1 / 2 : dimensions.width * 0.9,
                y: dimensions.width * 0.08 - dimensions.width * 0.04 / 2,
                fontSize: flag ? dimensions.width * 0.04 : 0,
                fill: isWin ? 'lime' : 'red',
                shadowBlur: flag ? 15 : 5,
            }}
        >
            {props =>
                <Group width={dimensions.width * 0.1}>
                    <animated.Text
                        {...props}
                        text={manageText()}
                    />
                </Group>
            }
        </Spring>
    }

    const manageText = () => {
        if (index === 51) return '';
        if (isWin) return `+${currentBet * 2}`;
        else return `-${currentBet}`;
    }

    const manageCircleColor = () => {
        if (flag && isWin && index < 51) {
            return 'seagreen';
        } else if (flag && !isWin && index < 51) {
            return 'indianred'
        } else if (!flag && index < 51) {
            return isWin ? 'seagreen' : 'indianred'
        } else return 'blue';
    }

    return (
        <Spring
            from={{
                x: 0,
                shadowBlur: 0,
                fill: isWin ? 'seagreen' : 'indianred'
            }}
            to={{
                x: flag && index < 51 ? dimensions.width * 0.9 - dimensions.width * 0.02 - 100 : dimensions.width * 0.9 - dimensions.width * 0.02,
                shadowBlur: flag && index < 51 ? 25 : 5,
                fill: manageCircleColor(),
                width: flag && index < 51 ? dimensions.width * 0.1 : 50,
                height: flag && index < 51 ? dimensions.width * 0.1 : 50
            }}
        >

            {props => (
                <Group
                    onMouseEnter={handleMouseEnter}
                    onMouseLeave={handleMouseLeave}
                    onClick={handleClick}
                >
                    <animated.Circle
                        {...props}
                        radius={dimensions.width < 1250 ? dimensions.width * 0.05 : dimensions.width * 0.035}
                        y={dimensions.width * 0.08}
                    />
                    {renderText()}
                </Group>
            )}
        </Spring>
    );
};


export default WinIndicator;