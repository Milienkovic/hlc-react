import React from 'react';
import {useSelector} from "react-redux";

import coinsImage from "../../../../../assets/images/coins.png";
import './CoinsInfo.css';

const CoinsInfo = ({coins}) => {

    const {isPlaying} = useSelector(state => ({
        isPlaying: state.game.isPlaying
    }));

    const renderCoinsInfo = () => {
        if (isPlaying)
            return <div className={'Coins'}>
                <img src={coinsImage} alt={'coins'}/>
                &nbsp;{coins}
            </div>
        return <></>
    }

    return (
        <>{renderCoinsInfo()}</>
    );
};

export default CoinsInfo;