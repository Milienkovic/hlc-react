import React, {useState} from 'react';
import {animated, useSpring} from "react-spring";
import './Logo.css';


const Logo = () => {
    const [flag, setFlag] = useState(false);

    const {transform, opacity} = useSpring({
        opacity: flag ? 1 : 0,
        transform: `perspective(600px) rotateX(${flag ? 180 : 0}deg)`,
        config: {mass: 5, tension: 500, friction: 80},
    });

    const handleClick = () => {
        setFlag(prevState => !prevState);
    }
    return (
        <div onClick={handleClick} className={'Logo-Container'}>
            <animated.div
                className={'Logo front'}
                style={{opacity: opacity.interpolate(o => 1 - o), transform}}
            />
            <animated.div
                className={'Logo back'}
                style={{opacity, transform: transform.interpolate(t => `${t} rotateX(180deg)`)}}
            />
        </div>
    );
};

export default Logo;