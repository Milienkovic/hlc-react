import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import Button from "../Button/Button";
import {gameOver, newGame, restart} from "../../../../core/redux/actions/gameActions";
import {newGamePlayer, resetPlayer} from "../../../../core/redux/actions/playerActions";
import useCards from "../../../../core/hooks/useCards";
import {displayBetControls} from "../../../../core/redux/actions/generalActions";
import CoinsInfo from "./CoinsInfo/CoinsInfo";

import Logo from "./Logo/Logo";
import './Stats.css';

const Stats = () => {
    const {coins, isPlaying} = useSelector(state => ({
        coins: state.player.coins,
        isPlaying: state.game.isPlaying
    }));

    const dispatch = useDispatch();
    const {shuffledDeck} = useCards();

    const handleRestart = () => {
        console.log('restart')
        const cards = shuffledDeck();
        dispatch(restart(cards));
        dispatch(resetPlayer());
        dispatch(displayBetControls(false));
    }

    const handleNewGame = () => {
        console.log('new game')
        const cards = shuffledDeck();
        if (coins > 0) {
            dispatch(newGame(cards));
            dispatch(newGamePlayer(coins));
        } else {
            dispatch(gameOver());
        }
        dispatch(displayBetControls(false));
    }

    return (
        <nav className={'Stats'}>
            <Logo/>
            <span className={'Spacer'}/>
            <CoinsInfo coins={coins}/>
            <span className={'Spacer'}/>
            <div className={'Links-Container'}>
                <Button
                    animate
                    onClick={handleNewGame}
                    color={'transparent'}
                    type={'button'}
                >New Game</Button>
                {isPlaying && <Button
                    animate
                    disabled={!isPlaying}
                    type={'button'}
                    color={'darkorchid'}
                    onClick={handleRestart}
                >Restart</Button>}
            </div>
        </nav>
    );
};

export default Stats;