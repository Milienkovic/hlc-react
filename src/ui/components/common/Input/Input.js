import React, {useEffect, useState} from 'react';
import './Input.css';

const Input = (props) => {
    const {id, name, initialValue, placeHolder, min, max, type, label, setBet, disabled} = props;
    const [value, setValue] = useState(initialValue || 1);
    const [blur, setBlur] = useState(false);

    useEffect(() => {
        if (value && value >= min && value <= max && blur)
            setBet(+value);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [value, blur]);

    const changeHandler = event => {
        const {value, name: targetName} = event.target;
        if (name === targetName)
            setValue(value);

        setTimeout(() => {
            setBlur(true);
        }, 1000);
    }

    const focusHandler = () => {
        setBet(0);
        setBlur(false);
        setValue('');
    }

    const blurHandler = event => {
        setBlur(true);
    }

    return (
        <div className={'Input-Container'}>
            <div className={'Input-Inner-Container'}><label htmlFor={id} className={'Label'}>{label}</label>
                <input
                    disabled={disabled}
                    onFocus={focusHandler}
                    onBlur={blurHandler}
                    className={'Input'}
                    id={id}
                    name={name}
                    value={value}
                    placeholder={placeHolder}
                    onChange={changeHandler}
                    min={min}
                    max={max}
                    type={type}
                />
            </div>
        </div>
    );
};

export default Input;