import React, {useEffect, useRef, useState} from 'react';
import {Image} from "react-konva";

const Img = (props) => {
    const {width, height, x, y, src} = props;
    const [image, setImage] = useState(null);
    const imageRef = useRef(null);

    useEffect(() => {
        loadImage();
        return () => {
            if (imageRef.current)
                imageRef.current.removeEventListener('load', handleLoad);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const loadImage = () => {
        const img = new window.Image();
        img.src = src;
        img.width = width;
        img.height = height;
        imageRef.current = img;
        imageRef.current.addEventListener('load', handleLoad);
    }

    const handleLoad = () => {
        setImage(imageRef.current);
    }

    const handleMouseEnter = event => {
        const container = event.target.getStage().container();
        container.style.cursor = 'pointer';
    }

    const handleMouseLeave = event => {
        const container = event.target.getStage().container();
        container.style.cursor = 'default';
    }
    return (
        <Image
            image={image}
            width={width}
            height={height}
            x={x}
            y={y}
            shadowColor={'orange'}
            shadowOffset={{x: 10, y: 10}}
            shadowBlur={10}
            shadowEnabled={true}
            shadowOpacity={0.9}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
        />
    );
};

export default Img;