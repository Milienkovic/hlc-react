import React, {useState} from 'react';
import {useSpring, animated} from "react-spring";

import './Button.css';

const Button = (props) => {
    const {type, onClick, disabled, children, color, id, animate} = props;
    const [shake, setShake] = useState(true);
    const {x} = useSpring({from: {x: 0}, x: shake ? 1 : 0, config: {duration: 1000}})
    return (
        <animated.div onClick={() => setShake(prevState => !prevState)}>
            <animated.button
                id={id}
                className={'Button'}
                type={type}
                onClick={onClick}
                disabled={disabled}
                style={animate ? {
                    backgroundColor: color,
                    transform: x.interpolate({
                        range: [0, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 1],
                        output: [1, 0.97, 0.92, 1.1, 0.92, 1.1, 1.03, 1]
                    })
                        .interpolate(x => `scale(${x})`)
                } : {backgroundColor: color}}
            >
                {children}
            </animated.button>
        </animated.div>
    );
};

export default Button;