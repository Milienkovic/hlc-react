import React from 'react';
import CardImage from "../Card/CardImage/CardImage";
import {Group} from "react-konva";
import {useSelector} from "react-redux";

const OpenCards = (props) => {
    let {openCards, cardWidth, openCardStartPosition} = props;

    const {dimensions} = useSelector(state => ({
        dimensions: state.general.dimensions,
    }));

    const dragEndHandler = () => {
        console.log('drag ended');
    }

    //todo
    let x = 1;

    return (
        <Group draggable={true} onDragEnd={dragEndHandler}>{
            openCards.map((card, index) =>
                <CardImage
                    key={card.name}
                    src={card.image}
                    width={80}
                    x={(index + 1) * cardWidth * 0.2 + cardWidth / 2 < dimensions.width ? (index + 1) * cardWidth * 0.2 : x++ * cardWidth * 0.2}
                    y={(index + 1) * cardWidth * 0.2 + cardWidth / 2 > dimensions.width ? dimensions.height * 0.66 : dimensions.height * 0.54}
                    isOpen
                    isHigher={card.isHigher}
                    openCardStartPosition={openCardStartPosition}
                />)
        }</Group>
    );
};

export default OpenCards;