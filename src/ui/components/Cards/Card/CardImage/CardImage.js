/* eslint-disable react-hooks/exhaustive-deps */

import React, {useEffect, useRef, useState} from 'react';
import {animated, Spring, useSpring} from "@react-spring/konva";

import back from '../../../../../assets/images/cards/card back.png';

const CardImage = (props) => {
    const {x, y, src, width, isOpen, isPrevious, isHigher, openCardStartPosition} = props;
    const imageRef = useRef(null);
    const [image, setImage] = useState(null);
    const cardRef = useRef(null);
    const animDuration = 1500;

    useEffect(() => {
        setAnimProps({xPos: x, yPos: y})
    }, [x, y]);

    const [animProps, setAnimProps] = useSpring(() => ({
        xPos: x,
        yPos: y,
        opacity: 1,
    }));


    const loadImage = () => {
        if (width) {
            const img = new window.Image();
            if (!isOpen && !isPrevious) img.src = back;
            else img.src = src;
            img.width = width;
            img.height = width * 1.4;
            imageRef.current = img;
            imageRef.current.addEventListener('load', handleLoad);
        }
    }

    useEffect(() => {
        loadImage();
        return () => {
            if (imageRef.current) {
                imageRef.current.removeEventListener('load', handleLoad);
            }
        }
    }, [src, width, imageRef]);

    const handleLoad = () => {
        setImage(imageRef.current);
    }

    const onMouseEnterHandler = event => {
        const container = event.target.getStage().container();
        container.style.cursor = 'pointer';
    }

    const onMouseLeaveHandler = event => {
        const container = event.target.getStage().container();
        container.style.cursor = 'default';
    }

    const renderImage = () => {
        if (width && image) {
            return <>
                <Spring
                    from={{
                        x: isOpen ? openCardStartPosition.x : 0,
                        y: isOpen ? openCardStartPosition.y : 0,
                    }}
                    to={{
                        x: animProps.xPos,
                        y: animProps.yPos,
                    }}
                    config={{duration: isOpen ? animDuration : 1000}}
                >
                    {props =>
                        (<animated.Image
                                {...props}
                                ref={cardRef}
                                onMouseEnter={onMouseEnterHandler}
                                onMouseLeave={onMouseLeaveHandler}
                                image={image}
                                shadowEnabled={true}
                                shadowColor={'grey'}
                                shadowBlur={10}
                                shadowOffset={{x: -10, y: 10}}
                                shadowOpacity={0.5}
                            />
                        )}
                </Spring>
                <Spring
                    from={{opacity: 0, fill: 'white'}}
                    to={{
                        opacity: animProps.opacity,
                        fill: isHigher ? 'lime' : 'red'
                    }}
                    config={{duration: 3000}}
                >
                    {props => (
                        <>
                            {isOpen && isHigher && <animated.Arrow
                                {...props}
                                points={[x + width * 0.2 / 2, y, x + width * 0.2 / 2, y - width * 0.2]}
                                x={0}
                                y={0}
                                pointerLength={width * 0.2}
                                pointerWidth={width * 0.2}
                                strokeWidth={2}
                            />}
                            {isOpen && !isHigher && <animated.Arrow
                                {...props}
                                points={[x + 2.5 + width * 0.2 / 2, y - width * 0.2, x + 2.5 + width * 0.2 / 2, y]}
                                x={0}
                                y={0}
                                pointerWidth={width * 0.2}
                                pointerLength={width * 0.2}
                                strokeWidth={2}
                            />}
                        </>
                    )}
                </Spring>
            </>
        }
    }

    return (
        <>
            {renderImage()}
        </>

    );
};

export default CardImage;