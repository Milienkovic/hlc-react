import React from 'react';
import {Group} from "react-konva";
import upArrow from './../../../../assets/images/up.png';
import downArrow from './../../../../assets/images/down.png';
import Img from "../../common/Img/Img";
const BetControls = (props) => {
    const {x, y} =props

    return (
        <Group  width={110} height={250} x={x} y={y} >
            <Img width={100} height={100} src={upArrow} x={0}  y={50}/>
            <Img width={100} height={100} src={downArrow} x={0} y={250}/>
        </Group>
    );
};

export default BetControls;