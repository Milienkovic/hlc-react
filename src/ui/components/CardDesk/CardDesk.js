/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import CardImage from "../Cards/Card/CardImage/CardImage";
import {Group, Text} from "react-konva";
import {useSelector} from "react-redux";
import OpenCards from "../Cards/OpenCards/OpenCards";
import WinIndicator from "../WinIndicator/WinIndicator";

import back from './../../../assets/images/cards/card back.png';

const CardDesk = (props) => {
    const {cards} = props;
    const [cardWidth, setCardWidth] = useState(225);

    const {dimensions, index, openCards, isPlaying} = useSelector(state => ({
        dimensions: state.general.dimensions,
        index: state.game.index,
        openCards: state.game.openCards,
        isPlaying: state.game.isPlaying
    }));

    useEffect(() => {
        if (dimensions.width > 0)
            setCardWidth(dimensions.width / 6)
    }, [dimensions.width]);

    const dragEndHandler = () => {
        console.log('dragging ended');
    }

    const renderDesk = () => {
        if (dimensions.width > 0 && cardWidth > 0 && isPlaying) {
            return <Group>
                {/*<Text text={cards[index - 1].name} fontSize={50}/>*/}
                <Group width={dimensions.width / 2} draggable onDragEnd={dragEndHandler}>
                    <CardImage
                        src={index === 0 ? back : cards[index].image}
                        x={dimensions.width / 2 - (cardWidth < 150 ? 150 : cardWidth) * 0.6 * 2}
                        y={(dimensions.height * 0.5) / 2 - (cardWidth * 1.4 * 0.6) / 2}
                        width={cardWidth < 150 ? 150 * 0.6 : cardWidth * 0.6}
                        isPrevious
                    />

                    <CardImage
                        width={cardWidth < 150 ? 150 : cardWidth * 0.8}
                        src={cards[index - 1].image}
                        x={dimensions.width / 2 - cardWidth * 0.8 / 2}
                        y={dimensions.height * 0.5 / 2 - cardWidth * 1.4 * 0.8 / 2}
                        isNext
                    />
                </Group>

                <OpenCards
                    cardWidth={cardWidth}
                    openCards={openCards}
                    openCardStartPosition={{
                        x: dimensions.width / 2 - (cardWidth < 150 ? 150 : cardWidth) * 0.6 * 2,
                        y: (dimensions.height * 0.5) / 2 - (cardWidth * 1.4 * 0.6) / 2
                    }}
                    openCardWidth={cardWidth < 150 ? (150 * 0.6) : (cardWidth * 0.6)}
                />
                <WinIndicator/>
            </Group>
        }
        return <Group width={250}>
            <Text text={'Start new game'} fontSize={40} x={dimensions.width / 2 - 125} y={dimensions.height / 2}/>
        </Group>
    }

    return (
        <>{renderDesk()}</>
    );
};

export default CardDesk;