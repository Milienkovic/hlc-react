/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useRef, useState} from 'react';
import {Group, Layer, Stage, Text} from "react-konva";
import {Provider, ReactReduxContext, useDispatch, useSelector} from "react-redux";

import CardDesk from "../CardDesk/CardDesk";
import BettingInput from "../BettingInput/BettingInput";
import useDimension from "../../../core/hooks/useDimension";
import {setDimensions} from "../../../core/redux/actions/generalActions";
import BettingControls from "../BettingControls/BettingControls/BettingControls";
import {placeBet} from "../../../core/redux/actions/playerActions";
import {newGame, setIndex, updateOpenCards} from "../../../core/redux/actions/gameActions";
import useCards from "../../../core/hooks/useCards";
import winSound from './../../../assets/sounds/win-sound.mp3';
import useSound from "use-sound";
import './CardBoard.css';

const CardBoard = () => {

    const [bet, setBet] = useState({amount: 10, guess: null, isPlaced: false});
    const boardRef = useRef(null);
    const dimensions = useDimension(boardRef);

    const dispatch = useDispatch();

    const {cards, isGameOver, index, isPlaying, isWin} = useSelector(state => ({
        cards: state.game.cards,
        isGameOver: state.game.isGameOver,
        dimensions: state.general.dimensions,
        isRefreshed: state.general.isRefreshed,
        index: state.game.index,
        isPlaying: state.game.isPlaying,
        isWin: state.player.isWin,
    }));

    const {shuffledDeck} = useCards();
    const [play] = useSound(winSound);
    const [played, setPlayed] = useState(false);

    useEffect(() => {
        if (isWin && !played) {
            play();
            setPlayed(true);
        }
    }, [isWin, play, played]);

    useEffect(() => {
        if (!isWin && index !== 51) {
            const cards = shuffledDeck();
            dispatch(newGame(cards));
        }
    }, [isWin, index]);

    useEffect(() => {
        if (bet.isPlaced) {
            const {amount, guess} = bet;
            const previousCard = cards[index];
            const nextCard = cards[index - 1];

            setPlayed(false);
            dispatch(placeBet({amount, guess, previousCard: previousCard.value, nextCard: nextCard.value}));

            if (index === 1) {
                const cards = shuffledDeck();
                dispatch(newGame(cards));
                return;
            }

            if (index > 0) {
                const card = {...cards[index], isHigher: previousCard.value > nextCard.value}
                dispatch(updateOpenCards(card));
                dispatch(setIndex(index - 1));
            }
            setBet(prevState => ({...prevState, isPlaced: false}));
        }
    }, [bet, index, isWin]);

    useEffect(() => {
        dispatch(setDimensions(dimensions));
    }, [dimensions]);

    const contextMenuHandler = event => {
        if (event.evt.button === 2) {
            event.evt.preventDefault();
        }
    }

    const handleInputSubmit = value => {
        setBet(prevState => ({
            ...prevState,
            amount: value,
        }));
    }

    const handleBetGuess = guess => {
        setBet(prevState => ({
            ...prevState,
            guess: guess,
            isPlaced: true,
        }));
    }

    const manageText = () => {
        if (isGameOver) return 'Game Over';
        if (!isPlaying) return 'Start New Game';
        return 'Please Wait';
    }

    const renderStage = () => {
        return (
            <ReactReduxContext.Consumer>
                {({store}) => <Stage
                    width={window.innerWidth}
                    height={window.innerHeight * 0.75}
                    onContextMenu={contextMenuHandler}>
                    <Provider store={store}>
                        {cards.length ?
                            <Layer>
                                <CardDesk cards={cards}/>
                            </Layer> : <Layer>
                                <Group width={250}><Text
                                    text={manageText()} fontSize={40}
                                    width={window.innerWidth}
                                    x={window.innerWidth / 2 - 125}
                                    y={window.innerHeight / 2}/></Group>
                            </Layer>}
                    </Provider>
                </Stage>}
            </ReactReduxContext.Consumer>
        );
    }

    return (
        <div className={'Container'} ref={boardRef}>
            <BettingInput onBetPlaced={handleInputSubmit}/>
            <BettingControls onGuessPlaced={handleBetGuess}/>
            {renderStage()}
        </div>
    );
};

export default CardBoard;