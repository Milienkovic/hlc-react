/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";

import Button from "../common/Button/Button";
import Input from "../common/Input/Input";
import {gameOver} from "../../../core/redux/actions/gameActions";
import {displayBetControls} from "../../../core/redux/actions/generalActions";

import './BettingInput.css';

const BettingInput = (props) => {

    const {currentBet, coins, isGameOver, displayBetControls: displayControls, isPlaying} = useSelector(state => ({
        currentBet: state.player.currentBet,
        coins: state.player.coins,
        isGameOver: state.game.isGameOver,
        displayBetControls: state.general.displayBetControls,
        isPlaying: state.game.isPlaying
    }));

    const {onBetPlaced} = props;
    const [bet, setBet] = useState(currentBet);

    const dispatch = useDispatch();

    useEffect(() => {
        if (coins === 0) {
            dispatch(gameOver());
        }
    }, [coins]);

    const handleClick = () => {
        if (bet > coins && !Number.isInteger(bet)) return;
        onBetPlaced(bet);
        dispatch(displayBetControls(true));
    }

    const manageDisabledButton = () => {
        return !bet || bet > coins || !Number.isInteger(bet) || isGameOver;
    }

    const contextMenuHandler = event => {
        event.preventDefault();
    }
    return (
        !displayControls && isPlaying && <div className={'Betting-Input-Container'} onContextMenu={contextMenuHandler}>
            <Input label={''} type={'number'} id={'bet'} name={'bet'} initialValue={bet}
                   setBet={setBet} min={Math.min(1, !!coins ? coins : 1)} max={coins} disabled={isGameOver}/>
            <Button
                disabled={manageDisabledButton()}
                type={'button'}
                onClick={handleClick}
            >Place Bet</Button>
        </div>
    );
};

export default BettingInput;