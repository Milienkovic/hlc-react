/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Button from "../../common/Button/Button";
import upArrow from './../../../../assets/images/up.png';
import downArrow from './../../../../assets/images/down.png';
import './BettingControls.css';
import BetOptions from "../../../../core/utils/BetOptions";
import {displayBetControls} from "../../../../core/redux/actions/generalActions";

const BettingControls = (props) => {
    const {displayBetControls: visible} = useSelector(state => ({
        displayBetControls: state.general.displayBetControls,
    }));

    const [guess, setGuess] = useState(null);
    const {onGuessPlaced} = props;
    const dispatch = useDispatch();

    useEffect(() => {
        if (guess !== null) {
            onGuessPlaced(guess);
            dispatch(displayBetControls(false));
        }
        return () => {
            setGuess(null);
        }
    }, [guess]);


    const handleLowerClick = () => {
        setGuess(BetOptions.LOWER);
    }

    const handleHigherClick = () => {
        setGuess(BetOptions.HIGHER);
    }

    const renderControls = () => {
        if (visible)
            return <div className={'Betting-Controls-Container'}>
                <Button
                    id={'higher'}
                    name={'higher'}
                    color={'red'}
                    disabled={guess}
                    onClick={handleHigherClick}
                    type={'button'}><img src={upArrow} alt={'higher'}/> &nbsp; Higher</Button>
                <Button
                    id={'lover'}
                    color={'lime'}
                    name={'lover'}
                    disabled={guess}
                    onClick={handleLowerClick}
                    type={'button'}><img src={downArrow} alt={'lower'}/>&nbsp; Lower</Button>
            </div>
    }
    return (
        <>{renderControls()}</>
    );
};

export default BettingControls;