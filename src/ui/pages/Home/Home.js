import React from 'react';
import CardBoard from "../../components/CardBoard/CardBoard";

import './Home.css';

const Home = () => {
    return (
        <div className={'Home'}>
            <CardBoard/>
        </div>
    );
};

export default Home;